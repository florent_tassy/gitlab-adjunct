![Mozilla Add-on rating](https://img.shields.io/amo/stars/gitlab-adjunct?style=flat-square)
![Mozilla Add-on version](https://img.shields.io/amo/v/gitlab-adjunct?style=flat-square)
![License](https://img.shields.io/gitlab/license/37614927?style=flat-square)
![Mozilla Add-on users](https://img.shields.io/amo/users/gitlab-adjunct?style=flat-square)


<img src="public/icon-dark.svg" alt="GitLab adjunct icon" height="150">

# GitLab adjunct 
GitLab adjunct is a Firefox add-on that helps to use GitLab.  

[![Get the add-on !](https://extensionworkshop.com/assets/img/documentation/publish/get-the-addon-178x60px.dad84b42.png)](https://addons.mozilla.org/firefox/addon/gitlab-adjunct/)

## How does it work ?
Once the add-on is installed, a new icon appears in Firefox toolbar. To use GitLab adjunct on another GitLab instance than gitlab.com:
- Go to the GitLab instance of your choice
- Click on the icon
- Request the permission by clicking the ❔ button
- It works!

![Firefox toolbar with GitLab adjunct button](images/gitlab-adjunct-example.gif)

As of now, GitLab adjunct automatically sum and display the estimated times and story points in each board:
![GitLab adjunct example](images/gitlab-adjunct-example.png)

### What permissions are needed ?
By default, GitLab adjunct does not ask any permission. You can grant domain-by-domain permissions for using your chosen GitLab instances.

### Does GitLab adjunct collect my data ?
No, and it will never do.

## Build from source

Building the add-on from source requires to have a working [Node.js](https://nodejs.org). In addition, some package.json scripts would work only in a "*nix" environment. The add-on can be built by running the following command from its root directory:  
```
npm ci  
npm run build:addon
```

## Legal notice
Firefox is a registered trademark of the Mozilla Foundation.  
Node.js is a trademark of the OpenJS Foundation.  
GITLAB is a trademark of GitLab Inc. in the United States and other countries and regions 

The above-mentioned trademarks are only used to refer to products.  
GitLab adjunct and its developer are not affiliated, sponsored nor endorsed by any of the above-mentioned organizations.  

## Change log
2.0.0 -> count "SP:xxx" tags to allow story points management, update dependencies
1.0.1 -> cleaner code, fixed typos, removed default gitlab.com permission
1.0.0 -> first release  
