import Counter from "../lib/Counter";


describe("Counter sums times correctly", () => {
    const counter: Counter = new Counter();

    test("At initialization", () => {
        expect(counter.sum).toBe("");
    });
});

describe("Counter sums times correctly", () => {
    const counter: Counter = new Counter();

    counter.updateCounter("1d");

    test("Adding 1 day", () => {
        expect(counter.sum).toBe("1d");
    });
});

describe("Counter sums times correctly", () => {
    const counter: Counter = new Counter();

    counter.updateCounter("1d");
    counter.updateCounter("8h");

    test("Adding 1 day, then 8 hours", () => {
        expect(counter.sum).toBe("2d");
    });
});

describe("Counter sums times correctly", () => {
    const counter: Counter = new Counter();

    counter.updateCounter("1d");
    counter.updateCounter("8h");
    counter.updateCounter("30minutes");

    test("Adding 1 day, then 8 hours, then 30 minutes", () => {
        expect(counter.sum).toBe("2d 30m");
    });
});

describe("Counter sums times correctly", () => {
    const counter: Counter = new Counter();

    counter.updateCounter("1d");
    counter.updateCounter("8h");
    counter.updateCounter("30minutes");
    counter.updateCounter("2weeks 3days");

    test("Adding 1 day, then 8 hours, then 30 minutes, then 2 weeks and 3 days", () => {
        expect(counter.sum).toBe("3w 30m");
    });
});

describe("Counter sums times correctly", () => {
    const counter: Counter = new Counter();

    counter.updateCounter("1d");
    counter.updateCounter("8h");
    counter.updateCounter("30minutes");
    counter.updateCounter("2weeks 3days");
    counter.updateCounter("1mo 1week 1day 7hours 30m");

    test("Adding 1 day, then 8 hours, then 30 minutes, then 2 weeks and 3 days, then 1 month, 1 week, 1 day, 7 hours and 30 minutes", () => {
        expect(counter.sum).toBe("2mo 2d");
    });
});
describe("SP count", () => {

    test("Adding SP:2 and SP:1 label should have have 3 SP total", () => {
        const counter: Counter = new Counter();

        counter.updateStoryPoints("SP:2")
        counter.updateStoryPoints("SP:1")
        expect(counter.totalStoryPoints).toBe(3);
    });
    test("Adding non SP labels should be 0 SP", () => {
        const counter: Counter = new Counter();

        counter.updateStoryPoints("turlututu")
        counter.updateStoryPoints("pouet")
        expect(counter.totalStoryPoints).toBe(0);
    });
    test("Adding SP labels with no numbers should be 0", () => {
        const counter: Counter = new Counter();

        counter.updateStoryPoints("SP:pouet")
        expect(counter.totalStoryPoints).toBe(0);
    });
});