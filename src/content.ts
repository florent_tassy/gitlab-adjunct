import Counter from "./lib/Counter";

function processTime(card: Element, counter: Counter) {
    const cardInfo = card.querySelector(".board-info-items");
    let timeInfo: NodeListOf<HTMLTimeElement> | null = null;
    if (cardInfo) {
        timeInfo = cardInfo.querySelectorAll("time");
    }
    if (timeInfo && timeInfo.length) {
        const time = timeInfo[0].innerText;
        if (timeInfo) {
            counter.updateCounter(time);
        }
    }
}

function processStoryPoints(card: Element, counter: Counter) {
    const labels:NodeListOf<HTMLSpanElement> = card.querySelectorAll(".board-card-labels .gl-label-text");
    if (labels) {
        for (const label of labels) {
            counter.updateStoryPoints(label.innerText);
        }
    }
}

function processCards(cards: HTMLCollection, counter: Counter): void {
    for (let card of Array.from(cards)) {
        processTime(card, counter);
        processStoryPoints(card, counter);
    }
}

function getTimeTag(counter: Counter): HTMLDivElement {
    const timeTag = document.createElement("div");
    timeTag.setAttribute("class", "gitlab-adjunct gl-font-sm issues-duration-badge gl-display-inline-flex gl-pr-2 no-drag gl-text-secondary");

    const valueTag = document.createElement("span");
    valueTag.innerText = counter.sum;

    timeTag.appendChild(valueTag);
    return timeTag;
}

function getTimeIconTag(): HTMLElement {
    const timeIcon = document.querySelector(".board-card-info-icon");
    if (!timeIcon) {
        throw new Error('Content script: document.querySelector(".board-card-info-icon") is null.');
    }
    const clonedIcon: HTMLElement = timeIcon.cloneNode(true) as HTMLElement;
    clonedIcon.setAttribute("class", "gitlab-adjunct " + clonedIcon.getAttribute("class"));
    return clonedIcon;
}
function getStoryPointsTag(counter: Counter): HTMLSpanElement {
    const storyPointsTag = document.createElement("span");
    storyPointsTag.setAttribute("class", "gitlab-adjunct gl-label js-no-trigger gl-mt-2 gl-mr-2 gl-label-sm gl-label-text-light");
    storyPointsTag.setAttribute("style", "--label-background-color: #6699cc; --label-inset-border: inset 0 0 0 1px #6699cc;");

    const valueTag = document.createElement("span");
    valueTag.setAttribute("class", "gl-label-text");
    valueTag.innerText = `SP:${counter.totalStoryPoints}`;

    storyPointsTag.appendChild(valueTag);
    return storyPointsTag;
}

function createTagsForCumultiveDurations(board: Element, counter: Counter) {
    if (counter.sum) {
        const boardTitle = board.querySelector(".board-title-text");

        if (boardTitle) {
            const timeTag = getTimeTag(counter);
            const timeIconTag = getTimeIconTag();
            boardTitle.after(timeTag);
            boardTitle.after(timeIconTag);
        }
    }
}
function createTagsForStoryPoints(board: Element, counter: Counter) {
    if (counter.totalStoryPoints) {
        const boardTitle = board.querySelector(".board-title-text");

        if (boardTitle) {
            const storyPointsTag = getStoryPointsTag(counter);
            boardTitle.after(storyPointsTag);
        }
    }
}

function deleteExtensionSpecificTags() {
    let tags: NodeListOf<HTMLElement> = document.querySelectorAll(".gitlab-adjunct");
    for (let tag of Array.from(tags)) {
        tag.remove();
    }
}

function addCumulativeDurations() {
    deleteExtensionSpecificTags();
    let boards: NodeListOf<HTMLElement> = document.querySelectorAll(".board");
    for (let board of Array.from(boards)) {
        let counter: Counter = new Counter();
        // Get cards
        let cards = board.getElementsByClassName("board-card");
        processCards(cards, counter);
        createTagsForCumultiveDurations(board, counter);
        createTagsForStoryPoints(board, counter);
    }
}

// Options for the observer (which mutations to observe)
const config = { attributes: false, childList: true, subtree: true };

function watchBoardsMutations() {
    let boardLists: HTMLCollection = document.getElementsByClassName("board-list");
    for (let boardList of Array.from(boardLists)) {
        // Create an observer instance linked to the callback function
        const observer = new MutationObserver(addCumulativeDurations);
        observer.observe(boardList, config);
    };
}

let nbAttempts: number = 0;
let url = window.location.href;

const intervalId = setInterval(() => {
    if (!url.includes("boards") || nbAttempts > 100) {
        clearInterval(intervalId);
        nbAttempts = 0;
    }

    let loaded: boolean = true;
    let boards: HTMLCollection = document.getElementsByClassName("board");
    if (!boards.length) {
        loaded = false;
        return;
    }
    for (let board of Array.from(boards)) {
        // Get cards
        let cards = board.getElementsByClassName("board-card");
        // Get issue count
        const itemCountWrapper = board.getElementsByClassName("item-count");
        if (!itemCountWrapper.length) {
            loaded = false;
            return;
        }
        let issueCount: number = Number.parseInt(itemCountWrapper[0].getElementsByTagName("span")[0].innerText);
        if (issueCount === 0 || (cards.length && issueCount >= cards.length)) {
            loaded = true;
        } else {
            loaded = false;
        }
        nbAttempts++;
    };
    if (loaded) {
        addCumulativeDurations();
        clearInterval(intervalId);
        watchBoardsMutations();
        nbAttempts = 0;
    }
}, 100);

