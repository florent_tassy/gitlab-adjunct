export default class Counter {
    months: number;
    weeks: number;
    days: number;
    hours: number;
    minutes: number;
    totalStoryPoints: number;

    constructor() {
        this.months = 0;
        this.weeks = 0;
        this.days = 0;
        this.hours = 0;
        this.minutes = 0;
        this.totalStoryPoints = 0;
    }

    // Conversion rates on https://docs.gitlab.com/ee/user/project/time_tracking.html#available-time-units
    get sum(): string {
        let sum: string = "";

        let extraHrs: number = 0;
        let extraDay: number = 0;
        let extraWek: number = 0;
        let extraMth: number = 0;

        if (this.minutes) {
            let lessThanHour: number = this.minutes % 60;
            if (lessThanHour) sum += ` ${lessThanHour}m`;
            extraHrs = (this.minutes - lessThanHour) / 60;
        }

        if (this.hours || extraHrs) {
            let totalHours: number = extraHrs + this.hours;
            let lessThanDay: number = totalHours % 8;
            if (lessThanDay) sum = ` ${lessThanDay}h` + sum;
            extraDay = (totalHours - lessThanDay) / 8;
        }

        if (this.days || extraDay) {
            let totalDays: number = extraDay + this.days;
            let lessThanWeek: number = totalDays % 5;
            if (lessThanWeek) sum = ` ${lessThanWeek}d` + sum;
            extraWek = (totalDays - lessThanWeek) / 5;
        }

        if (this.weeks || extraWek) {
            let totalWeeks: number = extraWek + this.weeks
            let lessThanMonth: number = totalWeeks % 4;
            if (lessThanMonth) sum = ` ${lessThanMonth}w` + sum;
            extraMth = (totalWeeks - lessThanMonth) / 4;
        }

        if (this.months || extraMth) {
            sum = `${(extraMth + this.months)}mo` + sum;
        }

        return sum.trim();
    }

    updateCounter(timeInfo: string): void {
        let timeComponents = timeInfo.split(" ");
        timeComponents.forEach((timeComponent) => {
            let unit = timeComponent.replace(/[0-9]/g, "");
            switch (unit) {
                case "m":
                case "minute":
                case "minutes":
                    this.minutes += +timeComponent.replace(unit, "");
                    break;
                case "h":
                case "hour":
                case "hours":
                    this.hours += +timeComponent.replace(unit, "");
                    break;
                case "d":
                case "day":
                case "days":
                    this.days += +timeComponent.replace(unit, "");
                    break;
                case "w":
                case "week":
                case "weeks":
                    this.weeks += +timeComponent.replace(unit, "");
                    break;
                case "mo":
                case "month":
                case "months":
                    this.months += +timeComponent.replace(unit, "");
                    break;
                default:
                    console.warn("Unit", unit, "not recognized in time component", timeComponent);
            }
        })
    }

    updateStoryPoints(labelName: string) {
        if (labelName.startsWith("SP:")) {
            const points = Number.parseInt(labelName.split(":")[1]);
            if (!Number.isNaN(points)) {
                this.totalStoryPoints += points;
            }
        }
    }
}