import Permission from "./lib/Permission";

async function register(): Promise<void> {
    let permissions: browser.permissions.AnyPermissions = await browser.permissions.getAll();
    const scripts: browser.scripting.RegisteredContentScript[] = [];

    if (permissions.origins) {
        permissions.origins.forEach((pattern: string) => {
            console.debug("Registering content script for", pattern);
            const permission = Permission.getPermissionFromPattern(pattern);
            scripts.push({
                id: permission.scriptId,
                matches: [permission.pattern],
                js: ["content.js"],
                runAt: "document_idle"
            });
        });
    }

    await browser.scripting.registerContentScripts(scripts);
}

register();
